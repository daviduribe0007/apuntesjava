package Clases;

public class Hilos {

    public static void forNoThreads() {


        for (int i = 0; i < 5; i++) {
            System.out.println("Programa 1 en la posicion: " + i);
        }
        System.out.println("");
        for (int j = 0; j < 5; j++) {
            System.out.println("Programa 2 en la posicion: " + j);
        }


    }

    public static void Threads() {
        Hilo1 primerHilo = new Hilo1();
        Thread segundoHilo = new Thread(new Hilo2());

        primerHilo.start();
        segundoHilo.start();
    }

}
